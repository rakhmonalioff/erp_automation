# erp_automation

## Getting started

ERP Project | Car Sales Automation

## Setup Locally

```
mkdir ERP-Project
cd ERP-Project
git clone https://gitlab.com/rakhmonalioff/erp_automation.git
cd erp_automation
```

## ENV Setup
For MacOS:
```
cd erp_automation
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
For Windows:
```
cd erp_automation
python -m venv venv
.\venv\Scripts\activate
pip install -r requirements.txt
```